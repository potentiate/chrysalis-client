///<reference path="node_modules/@types/node/index.d.ts"/>

import * as http from 'http';
import * as crypto from 'crypto';

export interface ChrysalisTopics {
    concept_layout: [{
        string: [number, number]
    }];
    topics: [{
        string: {
            coverage: number,
            context_weights: [
                number
                ],
            context_terms: [
                string
                ],
            rank: number,
            frequency: number,
            concept_variants: [
                string
                ],
            total_influence: number,
            name: string,
            context_variants: [
                [string]
                ],
            concept_terms: [
                string
                ],
            position: [
                number, number
                ],
            colour: string,
            context_concepts: [
                string
                ]
        }
    }];
}

export interface ChrysalisServerResponse {
    message?: string;
    status?: number|string;
    success?: boolean;
    url?: string;
    error?: { args: string[], failure_reason: string }
}

export interface ChrysalisAddDocumentResponse extends ChrysalisServerResponse {
    id?: number
}

export interface ChrysalisCreateTopicsResponse extends ChrysalisServerResponse {
    job_id?: string
}

export interface ChrysalisGetJobResponse extends ChrysalisServerResponse {
    details?: { topical_classification_url: string }
}

export interface ChrysalisClientConfiguration {
    hostname: string;
    port: number;
    api_key: string;
    secret_key: string;
}

export class ChrysalisClient {

    private static getAuthorizationHeader(configuration: ChrysalisClientConfiguration, http_options: { method: string, headers: { 'Date'?: string, 'Content-MD5'?: string, 'Content-Type'?: string } }): string {
        let string_to_sign: string = http_options.method + '\n';

        if ('Date' in http_options.headers && http_options.headers['Date']) {
            string_to_sign += http_options.headers['Date'] + '\n';
        } else {
            string_to_sign += '\n';
        }

        if ('Content-MD5' in http_options.headers && http_options.headers['Content-MD5']) {
            string_to_sign += http_options.headers['Content-MD5'] + '\n';
        } else {
            string_to_sign += '\n';
        }

        if ('Content-Type' in http_options.headers && http_options.headers['Content-Type']) {
            string_to_sign += http_options.headers['Content-Type'] + '\n';
        } else {
            string_to_sign += '\n';
        }

        string_to_sign += configuration.api_key;

        const signature = crypto.createHmac('sha1', configuration.secret_key).update(string_to_sign, 'utf8').digest('base64');

        return 'KAPICHE ' + configuration.api_key + ':' + signature;
    }

    private static getDateHeader(utc: boolean = true, offset:number = 0): string {

        if (utc) {
            return new Date().toISOString();
        } else {
            const my_date = new Date();
            my_date.setTime(my_date.getTime() + offset);
            return my_date.toISOString().replace('Z', '000');
        }
    }

    public static createIndex(configuration: ChrysalisClientConfiguration, index_name: string): Promise<ChrysalisServerResponse> {

        return new Promise((resolve, reject) => {

            const request_body_object = {schema: {document: {type: 'text'}}, settings: {fold_case: true}};

            const request_body_string = JSON.stringify(request_body_object);

            const request_options = {
                method: 'PUT',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name,
                headers: {
                    'Connection': 'keep-alive',
                    'Date': this.getDateHeader(),
                    'Content-MD5': crypto.createHash('md5').update(request_body_string).digest('hex'),
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(request_body_string)
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Creating index [' + index_name + ']. Body [' + request_body_string + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');

                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisServerResponse;
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            const error_message = 'Error creating index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            const error_message = 'Error creating index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object.success + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed creating index [' + index_name + '].');
                            resolve(response_body_object);
                        }

                    } catch (e) {
                        const error_message = 'Error creating index [' + index_name + ']. Response not JSON [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error creating index [' + index_name + ']. Error [' + error1 + '].';
                console.error();
                reject(new Error(error_message));
            });

            request.write(request_body_string);
            request.end();
        });
    }

    public static deleteIndex(configuration: ChrysalisClientConfiguration, index_name: string): Promise<ChrysalisServerResponse> {

        return new Promise((resolve, reject) => {

            const request_options = {
                method: 'DELETE',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name,
                headers: {
                    'Connection': 'close',
                    'Date': this.getDateHeader()
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Deleting index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');

                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisServerResponse;
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            const error_message = 'Error deleting index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            const error_message = 'Error deleting index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object.success + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed deleting index [' + index_name + '].');
                            resolve(response_body_object);
                        }

                    } catch (e) {
                        const error_message = 'Error deleting index [' + index_name + ']. Response not JSON [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error deleting index [' + index_name + ']. Error [' + error1 + '].';
                console.error();
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static addDocument(configuration: ChrysalisClientConfiguration, index_name: string, document_text: Buffer): Promise<ChrysalisAddDocumentResponse> {

        // Step 0. Cut the document up into lines
        const request_body_object: { document: string }[] = [];
        const document_text_lines = document_text.toString().split('\n');
        for (let i = 0, j = 0; i < document_text_lines.length; i++) {
            const line = document_text_lines[i].trim();
            if (line.length > 0) {
                request_body_object[j] = {document: line};
                j++;
            }
        }

        const request_body_string = JSON.stringify(request_body_object);

        return new Promise((resolve, reject) => {
            const request_options = {
                method: 'POST',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_docs',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': this.getDateHeader(),
                    'Content-MD5': crypto.createHash('md5').update(request_body_string).digest('hex'),
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(request_body_string)
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Adding document to index [' + index_name + ']. Document Text length [' + request_body_string.length + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');

                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisAddDocumentResponse;
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            const error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            const error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object['success'] + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed adding document to index [' + index_name + '].');
                            resolve(response_body_object);
                        }
                    } catch (e) {
                        const error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error adding document to index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.write(request_body_string);
            request.end();
        });
    }

    public static createTopics(configuration: ChrysalisClientConfiguration, index_name: string): Promise<ChrysalisCreateTopicsResponse> {

        return new Promise((resolve, reject) => {

            const request_options = {
                method: 'POST',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_topics/_framework/',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': this.getDateHeader()
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Creating topics on index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.request(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');

                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisCreateTopicsResponse;
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object && response_body_object.status && response_body_object.status !== 'submitted') {
                            const error_message = 'Error creating topics on index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed creating topics on index [' + index_name + ']. Job Id [' + response_body_object.job_id + '].');
                            resolve(response_body_object);
                        }

                    } catch (e) {
                        const error_message = 'Error creating topics on index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error creating topics on index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static getTopics(configuration: ChrysalisClientConfiguration, url_path: string): Promise<{ topic_name: string, term_names: string[] }[]> {

        return new Promise((resolve, reject) => {

            const request_options = {
                method: 'GET',
                host: configuration.hostname,
                port: configuration.port,
                path: url_path,
                headers: {
                    'Connection': 'keep-alive',
                    'Date': this.getDateHeader()
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Retrieving topics from URL path [' + url_path + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.get(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisTopics & { status: string | number, message: string };
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object.status && response_body_object.status !== '200') {
                            const error_message = 'Error retrieving topics from URL path [' + url_path + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed retrieving topics from URL path [' + url_path + ']. Found [' + response_body_string + '].');
                            const topics_and_terms: { topic_name: string, term_names: string[] }[] = [];
                            for (const topic_name of Object.keys(response_body_object.topics)) {
                                const topic = response_body_object.topics[topic_name];
                                topics_and_terms.push({topic_name: topic_name, term_names: topic.concept_terms})
                            }
                            resolve(topics_and_terms);
                        }
                    } catch (e) {
                        const error_message = 'Error retrieving topics from URL path [' + url_path + ']. Response [' + response_body_string + '], Error [' + e + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error_message = 'Error retrieving topics from URL path [' + url_path + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });

            request.end();
        });
    }

    public static getJob(configuration: ChrysalisClientConfiguration, index_name: string, job_id: string): Promise<ChrysalisGetJobResponse> {

        return new Promise((resolve, reject) => {

            const request_options = {
                method: 'GET',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_jobs/' + job_id + '/_progress',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': this.getDateHeader()
                }
            };

            request_options.headers['Authorization'] = this.getAuthorizationHeader(configuration, request_options);

            console.info('Retrieving job [' + job_id + '] from index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');

            const request = http.get(request_options, (response: http.IncomingMessage) => {
                response.setEncoding('utf8');
                let response_body_string = '';

                response.on('data', (chunk) => {
                    response_body_string += chunk;
                });

                response.on('end', () => {
                    let response_body_object: ChrysalisGetJobResponse;
                    try {
                        response_body_object = JSON.parse(response_body_string);

                        if (response_body_object.status && !(response_body_object.status === 200 || response_body_object.status === 'running' || response_body_object.status === 'completed')) {
                            const error_message = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        } else {
                            console.info('Completed retrieving job [' + job_id + '] from index [' + index_name + ']. Found [' + response_body_string + '].');
                            resolve(response_body_object);
                        }
                    } catch (e) {
                        const error_message = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', (error1: Error) => {
                const error2 = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error2);
                reject(new Error(error2));
            });

            request.end();
        });
    }

    public static processDocument(configuration: ChrysalisClientConfiguration, document_name: string, document_text: Buffer): Promise<{ topic_name: string, term_names: string[] }[]> {

        console.info('Processing document. Document Text length [' + document_text.length + '].');

        // Step 0. Common variables
        const index_name = crypto.createHash('md5').update(document_name).digest('hex');

        // Step 1. Create an index
        return new Promise((resolve, reject) => {
            ChrysalisClient.createIndex(configuration, index_name).then(() => {

                // Step 2. Add a document to the index
                ChrysalisClient.addDocument(configuration, index_name, document_text).then(() => {

                    // Step 3. Create topics
                    ChrysalisClient.createTopics(configuration, index_name).then(async (create_topics_response: { job_id: string }) => {

                        // Step 4. Wait for topics job to be completed
                        let topical_classification_url = null;
                        let do_break = false;
                        while (do_break === false) {
                            do_break = false;

                            ChrysalisClient.getJob(configuration, index_name, create_topics_response.job_id).then((get_job_response: { details: { topical_classification_url: string }, status: string | number }) => {
                                if (get_job_response.status === 'running') {
                                    do_break = false;
                                } else {
                                    topical_classification_url = get_job_response.details.topical_classification_url;
                                    do_break = true;
                                }

                            }, (reason: any) => {
                                const error_message = 'Cannot process document. Cannot retrieve job. Reason [' + reason + ']';
                                console.error(error_message);
                                ChrysalisClient.deleteIndex(configuration, index_name);
                                reject(new Error(error_message));
                                do_break = true;
                            });

                            if (do_break) {
                                break;
                            }

                            await new Promise((resolve) => {
                                setTimeout(resolve, 1000);
                            });
                        }

                        // Step 5. Get Topics
                        if (topical_classification_url !== null) {
                            ChrysalisClient.getTopics(configuration, topical_classification_url).then((get_topics_response: any) => {
                                ChrysalisClient.deleteIndex(configuration, index_name);
                                resolve(get_topics_response);
                            }, (reason: any) => {
                                const error_message = 'Cannot process document. Cannot get topics. Reason [' + reason + ']';
                                console.error(error_message);
                                ChrysalisClient.deleteIndex(configuration, index_name);
                                reject(new Error(error_message));
                            });
                        }

                    }, (reason: any) => {
                        const error_message = 'Cannot process document. Cannot create topics. Reason [' + reason + '].';
                        console.error(error_message);
                        ChrysalisClient.deleteIndex(configuration, index_name);
                        reject(new Error(error_message));
                    });

                }, (reason: any) => {
                    const error_message = 'Cannot process document. Cannot add document to index. Reason [' + reason + '].';
                    console.error(error_message);
                    ChrysalisClient.deleteIndex(configuration, index_name);
                    reject(new Error(error_message));
                });

            }, (reason: any) => {
                const error_message = 'Cannot process document. Cannot create index. Reason [' + reason + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
        });
    }
}
