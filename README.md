A NodeJS client to connect to the Chrysalis Text Analytics engine by Kapiche

Kapiche finds actionable insights using proven Text Analytics technology. 

A “Topic Model” is a natural language methodology of finding and tracing clusters of words in large bodies of text. It identifies and tracks patterns and relationships in a set of unstructured data

These clusters of words, its frequency in the documents and the co-occurrence between them are all considered by the Kapiche algorithm to determine which relationships are important, cut out noise and hence which topics can be considered influential.

For example, the word dog might influence the presence of a host of other terms like kennel, fleas, bark and lead in a document. The measure of individual word frequency and its influence on other words form the basis of the algorithm 

For access to the Chrysalis server please contact sales at [Potentiate](http://potentiate.com)

## Install

```
$ npm install chrysalis-client
```


Usage
=====

```js
import {ChrysalisClient, ChrysalisClientConfiguration} from 'chrysalis-client';
// or:
var chrysalis_client = require("chrysalis-client").ChrysalisClient;
```

To retrieve the topics within a document

```js
var chrysalis_client_config = {
    hostname: 'myhostname.com', //Replace with the provided hostname. Do not add protocol (http or https)
    port: 8302, //Default is 8302
    api_key: 'my_api_key', //Replace with the provided api key
    secret_key: 'my_secret_key' //Replace with the provided secret key
};
var document_name = '/tmp/myfile.txt';
var document_text = Buffer.from('This is the raw text from my document', 'utf8');
chrysalis_client.processDocument(chrysalis_client_config, document_name, document_text).then(function (topics_and_terms) {
    //Do something here with the topics and terms
}, function (error) {
 console.error('Error processing file [' + object.Key + ']. Chrysalis cannot process document. Error [' + error + '].');
});
```