"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var crypto = require("crypto");
var ChrysalisClient = (function () {
    function ChrysalisClient() {
    }
    ChrysalisClient.getAuthorizationHeader = function (configuration, http_options) {
        var string_to_sign = http_options.method + '\n';
        if ('Date' in http_options.headers && http_options.headers['Date']) {
            string_to_sign += http_options.headers['Date'] + '\n';
        }
        else {
            string_to_sign += '\n';
        }
        if ('Content-MD5' in http_options.headers && http_options.headers['Content-MD5']) {
            string_to_sign += http_options.headers['Content-MD5'] + '\n';
        }
        else {
            string_to_sign += '\n';
        }
        if ('Content-Type' in http_options.headers && http_options.headers['Content-Type']) {
            string_to_sign += http_options.headers['Content-Type'] + '\n';
        }
        else {
            string_to_sign += '\n';
        }
        string_to_sign += configuration.api_key;
        var signature = crypto.createHmac('sha1', configuration.secret_key).update(string_to_sign, 'utf8').digest('base64');
        return 'KAPICHE ' + configuration.api_key + ':' + signature;
    };
    ChrysalisClient.getDateHeader = function (utc, offset) {
        if (utc === void 0) { utc = true; }
        if (offset === void 0) { offset = 0; }
        if (utc) {
            return new Date().toISOString();
        }
        else {
            var my_date = new Date();
            my_date.setTime(my_date.getTime() + offset);
            return my_date.toISOString().replace('Z', '000');
        }
    };
    ChrysalisClient.createIndex = function (configuration, index_name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var request_body_object = { schema: { document: { type: 'text' } }, settings: { fold_case: true } };
            var request_body_string = JSON.stringify(request_body_object);
            var request_options = {
                method: 'PUT',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name,
                headers: {
                    'Connection': 'keep-alive',
                    'Date': _this.getDateHeader(),
                    'Content-MD5': crypto.createHash('md5').update(request_body_string).digest('hex'),
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(request_body_string)
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Creating index [' + index_name + ']. Body [' + request_body_string + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.request(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            var error_message = 'Error creating index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            var error_message = 'Error creating index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object.success + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed creating index [' + index_name + '].');
                            resolve(response_body_object);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error creating index [' + index_name + ']. Response not JSON [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error creating index [' + index_name + ']. Error [' + error1 + '].';
                console.error();
                reject(new Error(error_message));
            });
            request.write(request_body_string);
            request.end();
        });
    };
    ChrysalisClient.deleteIndex = function (configuration, index_name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var request_options = {
                method: 'DELETE',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name,
                headers: {
                    'Connection': 'close',
                    'Date': _this.getDateHeader()
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Deleting index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.request(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            var error_message = 'Error deleting index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            var error_message = 'Error deleting index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object.success + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed deleting index [' + index_name + '].');
                            resolve(response_body_object);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error deleting index [' + index_name + ']. Response not JSON [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error deleting index [' + index_name + ']. Error [' + error1 + '].';
                console.error();
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    ChrysalisClient.addDocument = function (configuration, index_name, document_text) {
        var _this = this;
        var request_body_object = [];
        var document_text_lines = document_text.toString().split('\n');
        for (var i = 0, j = 0; i < document_text_lines.length; i++) {
            var line = document_text_lines[i].trim();
            if (line.length > 0) {
                request_body_object[j] = { document: line };
                j++;
            }
        }
        var request_body_string = JSON.stringify(request_body_object);
        return new Promise(function (resolve, reject) {
            var request_options = {
                method: 'POST',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_docs',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': _this.getDateHeader(),
                    'Content-MD5': crypto.createHash('md5').update(request_body_string).digest('hex'),
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(request_body_string)
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Adding document to index [' + index_name + ']. Document Text length [' + request_body_string.length + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.request(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object && response_body_object.status && response_body_object.status !== 200) {
                            var error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else if (response_body_object && response_body_object.success && response_body_object.success !== true) {
                            var error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_object.message + '], Success [' + response_body_object['success'] + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed adding document to index [' + index_name + '].');
                            resolve(response_body_object);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error adding document to index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error adding document to index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.write(request_body_string);
            request.end();
        });
    };
    ChrysalisClient.createTopics = function (configuration, index_name) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var request_options = {
                method: 'POST',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_topics/_framework/',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': _this.getDateHeader()
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Creating topics on index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.request(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object && response_body_object.status && response_body_object.status !== 'submitted') {
                            var error_message = 'Error creating topics on index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed creating topics on index [' + index_name + ']. Job Id [' + response_body_object.job_id + '].');
                            resolve(response_body_object);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error creating topics on index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error creating topics on index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    ChrysalisClient.getTopics = function (configuration, url_path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var request_options = {
                method: 'GET',
                host: configuration.hostname,
                port: configuration.port,
                path: url_path,
                headers: {
                    'Connection': 'keep-alive',
                    'Date': _this.getDateHeader()
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Retrieving topics from URL path [' + url_path + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.get(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object.status && response_body_object.status !== '200') {
                            var error_message = 'Error retrieving topics from URL path [' + url_path + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed retrieving topics from URL path [' + url_path + ']. Found [' + response_body_string + '].');
                            var topics_and_terms = [];
                            for (var _i = 0, _a = Object.keys(response_body_object.topics); _i < _a.length; _i++) {
                                var topic_name = _a[_i];
                                var topic = response_body_object.topics[topic_name];
                                topics_and_terms.push({ topic_name: topic_name, term_names: topic.concept_terms });
                            }
                            resolve(topics_and_terms);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error retrieving topics from URL path [' + url_path + ']. Response [' + response_body_string + '], Error [' + e + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error_message = 'Error retrieving topics from URL path [' + url_path + ']. Error [' + error1 + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
            request.end();
        });
    };
    ChrysalisClient.getJob = function (configuration, index_name, job_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var request_options = {
                method: 'GET',
                host: configuration.hostname,
                port: configuration.port,
                path: '/rest/' + index_name + '/_jobs/' + job_id + '/_progress',
                headers: {
                    'Connection': 'keep-alive',
                    'Date': _this.getDateHeader()
                }
            };
            request_options.headers['Authorization'] = _this.getAuthorizationHeader(configuration, request_options);
            console.info('Retrieving job [' + job_id + '] from index [' + index_name + ']. Options [' + JSON.stringify(request_options) + '].');
            var request = http.get(request_options, function (response) {
                response.setEncoding('utf8');
                var response_body_string = '';
                response.on('data', function (chunk) {
                    response_body_string += chunk;
                });
                response.on('end', function () {
                    var response_body_object;
                    try {
                        response_body_object = JSON.parse(response_body_string);
                        if (response_body_object.status && !(response_body_object.status === 200 || response_body_object.status === 'running' || response_body_object.status === 'completed')) {
                            var error_message = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Response [' + response_body_object.message + '], Status [' + response_body_object.status + ']';
                            console.error(error_message);
                            reject(new Error(error_message));
                        }
                        else {
                            console.info('Completed retrieving job [' + job_id + '] from index [' + index_name + ']. Found [' + response_body_string + '].');
                            resolve(response_body_object);
                        }
                    }
                    catch (e) {
                        var error_message = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Response [' + response_body_string + ']';
                        console.error(error_message);
                        reject(new Error(error_message));
                    }
                });
            }).on('error', function (error1) {
                var error2 = 'Error retrieving job [' + job_id + '] from index [' + index_name + ']. Error [' + error1 + '].';
                console.error(error2);
                reject(new Error(error2));
            });
            request.end();
        });
    };
    ChrysalisClient.processDocument = function (configuration, document_name, document_text) {
        var _this = this;
        console.info('Processing document. Document Text length [' + document_text.length + '].');
        var index_name = crypto.createHash('md5').update(document_name).digest('hex');
        return new Promise(function (resolve, reject) {
            ChrysalisClient.createIndex(configuration, index_name).then(function () {
                ChrysalisClient.addDocument(configuration, index_name, document_text).then(function () {
                    ChrysalisClient.createTopics(configuration, index_name).then(function (create_topics_response) { return __awaiter(_this, void 0, void 0, function () {
                        var topical_classification_url, do_break;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    topical_classification_url = null;
                                    do_break = false;
                                    _a.label = 1;
                                case 1:
                                    if (!(do_break === false)) return [3, 3];
                                    do_break = false;
                                    ChrysalisClient.getJob(configuration, index_name, create_topics_response.job_id).then(function (get_job_response) {
                                        if (get_job_response.status === 'running') {
                                            do_break = false;
                                        }
                                        else {
                                            topical_classification_url = get_job_response.details.topical_classification_url;
                                            do_break = true;
                                        }
                                    }, function (reason) {
                                        var error_message = 'Cannot process document. Cannot retrieve job. Reason [' + reason + ']';
                                        console.error(error_message);
                                        ChrysalisClient.deleteIndex(configuration, index_name);
                                        reject(new Error(error_message));
                                        do_break = true;
                                    });
                                    if (do_break) {
                                        return [3, 3];
                                    }
                                    return [4, new Promise(function (resolve) {
                                            setTimeout(resolve, 1000);
                                        })];
                                case 2:
                                    _a.sent();
                                    return [3, 1];
                                case 3:
                                    if (topical_classification_url !== null) {
                                        ChrysalisClient.getTopics(configuration, topical_classification_url).then(function (get_topics_response) {
                                            ChrysalisClient.deleteIndex(configuration, index_name);
                                            resolve(get_topics_response);
                                        }, function (reason) {
                                            var error_message = 'Cannot process document. Cannot get topics. Reason [' + reason + ']';
                                            console.error(error_message);
                                            ChrysalisClient.deleteIndex(configuration, index_name);
                                            reject(new Error(error_message));
                                        });
                                    }
                                    return [2];
                            }
                        });
                    }); }, function (reason) {
                        var error_message = 'Cannot process document. Cannot create topics. Reason [' + reason + '].';
                        console.error(error_message);
                        ChrysalisClient.deleteIndex(configuration, index_name);
                        reject(new Error(error_message));
                    });
                }, function (reason) {
                    var error_message = 'Cannot process document. Cannot add document to index. Reason [' + reason + '].';
                    console.error(error_message);
                    ChrysalisClient.deleteIndex(configuration, index_name);
                    reject(new Error(error_message));
                });
            }, function (reason) {
                var error_message = 'Cannot process document. Cannot create index. Reason [' + reason + '].';
                console.error(error_message);
                reject(new Error(error_message));
            });
        });
    };
    return ChrysalisClient;
}());
exports.ChrysalisClient = ChrysalisClient;
//# sourceMappingURL=index.js.map